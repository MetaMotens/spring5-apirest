package com.roberto.backend.controllers;

import com.roberto.backend.entities.Client;
import com.roberto.backend.models.ClientModel;
import com.roberto.backend.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin({ "http://localhost:4200" })
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/clientes")
    public List<Client> findAll(){
        return clientService.findAll();
    }

    @GetMapping("/clientes/{id}")
    public Client findById(@PathVariable Long id){
        return clientService.findById(id);
    }

    @PostMapping("/clientes")
    public Client create(@RequestBody Client client){
        return clientService.update(client);
    }

    @PutMapping("/clientes/{id}")
    public Client update(@RequestBody Client client, @PathVariable Long id){
        Client actualClient = clientService.findById(id);

        actualClient.setNombre(client.getNombre());
        actualClient.setApellido(client.getApellido());
        actualClient.setEmail(client.getEmail());

        return clientService.update(actualClient);
    }

    @DeleteMapping("/clientes/{id}")
    public void delete(@PathVariable Long id){
        clientService.delete(id);
    }
}
