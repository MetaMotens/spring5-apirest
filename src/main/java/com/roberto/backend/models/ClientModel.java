package com.roberto.backend.models;

import com.roberto.backend.entities.Client;

import javax.persistence.*;
import java.util.Date;

public class ClientModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;
    private String apellido;
    private String email;

    @Column(name = "create_at")
    private Date createAt;

    @PrePersist
    public void create(){
        this.createAt = new Date();
    }

    public static ClientModel from (Client client){
        ClientModel clientModel = new ClientModel();

        clientModel.setId(client.getId());
        clientModel.setNombre(client.getNombre());
        clientModel.setApellido(client.getApellido());
        clientModel.setEmail(client.getEmail());
        clientModel.setCreateAt(client.getCreateAt());

        return clientModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}
