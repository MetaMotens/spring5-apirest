package com.roberto.backend.services.impl;

import com.roberto.backend.entities.Client;
import com.roberto.backend.repositories.ClientRepository;
import com.roberto.backend.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl( ClientRepository clientRepository ) { this.clientRepository = clientRepository; }

    @Override
    public List<Client> findAll(){
        return (List<Client>) clientRepository.findAll();
    }

    @Transactional
    public Client findById(Long id){
        return clientRepository.findById(id).orElse(null);
    }

    @Transactional
    public Client update(Client client) {
        return clientRepository.save(client);
    }

    @Transactional
    public void delete(Long id) {
        clientRepository.deleteById(id);
    }


}
