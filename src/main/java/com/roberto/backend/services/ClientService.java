package com.roberto.backend.services;

import com.roberto.backend.entities.Client;

import java.util.List;

public interface ClientService {

    public List<Client> findAll();

    public Client findById(Long id);

    public Client update(Client client);

    public void delete(Long id);

}
